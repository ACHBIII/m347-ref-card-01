
# App Ref. Card 01
Standalone Spring Boot Application

---
title: App Ref. Card 01
author: Aabid Ahamed, BBW
date: 24. Mai 2024
---


Link zur Übersicht:<br/>
https://gitlab.com/bbwrl/m347-ref-card-overview


## Installation der benötigten Werkzeuge

Maven Tutorial for Beginners<br/>
https://www.simplilearn.com/tutorials/maven-tutorial



### Projekt bauen und starten
Die Ausführung der Befehle erfolgt im Projektordner
Builden mit Maven<br/>
```$ mvn package```

Das Projekt wird gebaut und die entsprechende Jar-Datei im Ordner Target erstellt (Artefakt).
Die erstellte Datei kann nun direkt mit Java gestartet werden.<br/>
```$ java -jar target/app-refcard-01-0.0.1-SNAPSHOT.jar```

Die App kann im Browser unter der URL http://localhost:8080 betrachtet werden.


### Voraussetzungen

Bevor Sie beginnen, stellen Sie sicher, dass die folgenden Voraussetzungen erfüllt sind:

- **Betriebssystem:** Das Projekt ist plattformunabhängig, wurde jedoch auf Windows, macOS und Linux getestet.
- **Java:** JDK 11 oder höher ist erforderlich. Sie können es von [Oracle](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html) oder [OpenJDK](https://openjdk.java.net/install/) herunterladen.
- **Maven:** Apache Maven 3.6.3 oder höher. Sie können es von [Mavens offizieller Website](https://maven.apache.org/download.cgi) herunterladen.
- **Docker:** Docker sollte installiert sein, wenn Sie die Anwendung in einem Container ausführen möchten. Sie können es von [Dockers offizieller Website](https://www.docker.com/get-started) herunterladen.

### Installation

Folgen Sie diesen Schritten, um das Projekt zum Laufen zu bringen:

1. **Klonen Sie das Repository**

   ```bash
   git clone https://gitlab.com/IhrBenutzername/Ref-Card-03.git
   cd Ref-Card-01

2. Erstellen Sie das Docker-Image

   ```bash
   docker build -t ref-card-03 .

3. Container erstellen und ausführen

   ```bash
   docker run -p 8080:8080 ref-card-03





